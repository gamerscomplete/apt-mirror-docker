#!/bin/bash

# Write the configuration from the environment variable to the mirror.list file
echo "$MIRROR_CONFIG" > /etc/apt/mirror.list

# Run apt-mirror
apt-mirror
