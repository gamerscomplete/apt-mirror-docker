# Local APT Mirror

This project sets up a local APT mirror for Ubuntu 22.04 and Debian 10 & 11, including i386 repositories for Ubuntu. It uses Docker and Docker Compose to run two separate containers: one for serving the mirror using Nginx and another for updating the packages in the mirror.

## Prerequisites

- Docker
- Docker Compose

## Setup

1. Clone the repository or copy the provided files into a new directory.
2. Navigate to the project directory in your terminal.
3. Run `docker-compose up -d nginx` to build and start the Nginx container. This will also create the `mirror-data` directory if it doesn't exist.
4. To update the mirror, run `docker-compose run --rm apt_mirror`. This will build and run the apt_mirror container to download and update the packages in the `mirror-data` directory. The `--rm` flag ensures the container is removed after the update is completed.

## Usage

Once the Nginx container is running, you can access the local APT mirror at `http://localhost/`. To use the mirror on your Ubuntu or Debian system, update the `/etc/apt/sources.list` file to point to your local mirror.

For example, change:
```
deb http://archive.ubuntu.com/ubuntu/ jammy main restricted
```

to:

```
deb http://localhost/ubuntu/ jammy main restricted
```

Make sure to update the sources for both Ubuntu and Debian, depending on your needs.

## Stopping and Removing Containers

To stop and remove the containers, run:

docker-compose down

Note that the mirrored data in the `mirror-data` directory will persist even after the containers are removed, as it is stored in a local directory on the host machine.
